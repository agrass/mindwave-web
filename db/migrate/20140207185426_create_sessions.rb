class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :name
      t.string :description
      t.string :photo
      t.integer :data_id
      t.timestamps
    end
  end
end
