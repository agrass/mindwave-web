class AddTimeToDatum < ActiveRecord::Migration
  def change
    add_column :data, :time, :datetime
  end
end
