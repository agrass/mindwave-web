class DataController < ApplicationController
  # GET /data
  # GET /data.json
  def index 
    @abc = Hash.new
    @abc = { "a" => 0, "b" => 1 , "c" => 2, "d" => 3 , "e" => 4, "f" => 5, "g" => 6, "h" => 7, "i" => 8, "j" => 9, "k" => 10, "l" => 11, "m" => 12, "n" => 13, "o" => 14, "p" => 15, "q" => 16, "r" => 17, "s" => 18, "t" => 19, "u" => 20, "v" => 21, "w" => 22, "x" => 23, "y" => 24, "z" => 25 }
    @med = Hash.new
    @att = Hash.new
    @bk = Hash.new
    @let = Hash.new
    @num = []
    if params[:word]
      params[:word].split("").each do |z|
        @num.push @abc[z]
      end
    end 
    Datum.find_all_by_name_and_session("med", params[:session]).each do |dat|
      @med[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("att", params[:session]).each do |dat|
      @att[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("let", params[:session]).each do |dat|
      if @num.include? dat.value or params[:word] == nil
        @let[dat.time] = dat.value
      end
    end
    Datum.find_all_by_name_and_session("bk", params[:session]).each do |dat|      
       @bk[dat.time] = dat.value     
    end


    respond_to do |format|
      format.html # index.html.erb      
    end
  end

  def index2
    @delta = Hash.new
    @halpha = Hash.new
    @lalpha = Hash.new
    @lbeta = Hash.new
    @lgama = Hash.new
    @mgama = Hash.new
    @theta = Hash.new
    @let = Hash.new
    Datum.find_all_by_name_and_session("delta", params[:session]).each do |dat|
      @delta[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("halpha", params[:session]).each do |dat|
      @halpha[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("lalpha", params[:session]).each do |dat|
      @lalpha[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("lbeta", params[:session]).each do |dat|
      @lbeta[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("lgama", params[:session]).each do |dat|
      @lgama[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("mgama", params[:session]).each do |dat|
      @mgama[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("theta", params[:session]).each do |dat|
      @theta[dat.time] = dat.value
    end
    Datum.find_all_by_name_and_session("let", params[:session]).each do |dat|
      @let[dat.time] = dat.value
    end
  end

  # GET /data/1
  # GET /data/1.json
  def show
    @datum = Datum.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @datum }
    end
  end

  # GET /data/new
  # GET /data/new.json
  def new
    @datum = Datum.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @datum }
    end
  end

  # GET /data/1/edit
  def edit
    @datum = Datum.find(params[:id])
  end

  def send_data
    @session = Session.last.data_id
    if params[:delta] != nil
      @datum = Datum.new(:value => params[:delta], :name => "delta", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:halpha], :name => "halpha", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:hbeta], :name => "hbeta", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:lalpha], :name => "lalpha", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:lbeta], :name => "lbeta", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:lgama], :name => "lgama", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:mgama], :name => "mgama", :session => @session, :time => params[:hora]  )
      Datum.create(:value => params[:theta], :name => "theta", :session => @session, :time => params[:hora]  )
    else
      @datum = Datum.new(:value => params[:valor], :name => params[:name], :session => @session, :time => params[:hora]  )
    end
    if @datum.save
      render json: @datum
    else
      render json: @datum.errors
    end
  end

  # POST /data
  # POST /data.json
  def create
    @datum = Datum.new(params[:datum])

    respond_to do |format|
      if @datum.save
        format.html { redirect_to @datum, notice: 'Datum was successfully created.' }
        format.json { render json: @datum, status: :created, location: @datum }
      else
        format.html { render action: "new" }
        format.json { render json: @datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /data/1
  # PUT /data/1.json
  def update
    @datum = Datum.find(params[:id])

    respond_to do |format|
      if @datum.update_attributes(params[:datum])
        format.html { redirect_to @datum, notice: 'Datum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data/1
  # DELETE /data/1.json
  def destroy
    @datum = Datum.find(params[:id])
    @datum.destroy

    respond_to do |format|
      format.html { redirect_to data_url }
      format.json { head :no_content }
    end
  end
end
